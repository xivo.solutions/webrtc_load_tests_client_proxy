const { recordCallEvent } = require("./reporter");
const { extractUrlParams, agregateClientEvent, notifyPendingRequests, initScenario, scenarioFactory,  } = require("./utils");

/**
 * This object stores current scenarios configurations objects, that contain informations
 * needed by the client proxy, in order to orchestrate the calls among browser clients. 
 * For example it stores callbacks, which resolve when last call/registration is made.
 * The last call/registration is identifyed using phone events (eg. PhoneStatusEvent),
 * which the puppeteer controlled browsers are continuosly sending to this proxy.
 * 
 * In the call pair mode, there can be multiple scenarios, that is why there is an array 
 * of scenarios.
 */
let ongoingScenarios = []

const clientEvent = (req, res) => {
    const { iteration, userType, username, partyNumber, eventType, sipCallId } = extractUrlParams(req);
    const { currentCall, callEvent } = agregateClientEvent(userType, eventType, username, partyNumber, sipCallId)
    recordCallEvent(iteration, callEvent, currentCall);
    notifyPendingRequests(ongoingScenarios, callEvent, currentCall, iteration);
    res.end();
    return;
};

/**
 * This function creates scenario objects in ONGOING_SCENARIO objects in order 
 * to store callbacks which resolve when last call/registration is made and other.
 */
const setupScenario = (req, res) => {
    let scenarioBase = scenarioFactory(req);
    const scenario = initScenario(req, res, scenarioBase);
    ongoingScenarios.push(scenario)
    console.log(`ClientProxy: Finished storing callback; Current number of callbacks: ${ongoingScenarios[0].callbacks.length};`);
    return;
}

module.exports = {
    clientEvent: clientEvent,
    setupScenario: setupScenario,
}