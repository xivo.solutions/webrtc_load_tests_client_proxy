const { recordStopIteration, recordStartIteration, clearReport, verifyPhoneEventExistsOnBothSides } = require("./reporter");

const LOADTEST_URL = process.env.LOADTEST_URL;
const CLIENT_PROXY_PORT = parseInt(process.env.CLIENT_PROXY_PORT);

const getUrlParams = (request) => {
    const url = new URL(`http://${LOADTEST_URL}:${CLIENT_PROXY_PORT}${request.url}`);
    return url.searchParams;
}

const extractUrlParams = (request) => {
    const url = new URL(`http://${LOADTEST_URL}:${CLIENT_PROXY_PORT}${request.url}`);
    const urlEntries = url.searchParams.entries()
    result = {};
    for (const [key, value] of urlEntries) {
        result[key] = value;
    }
    return result;
}

const getIteration = (req) => {
    const urlParams = getUrlParams(req);
    const iteration = urlParams.get('iteration');
    return iteration;
}

const scenarioFactory = (request) => {
    const {scenarioName, numOfUsers, numOfCalls, iteration, firstNumber, lastExpectedCall, condition} = extractUrlParams(request);
    return {
        scenarioName: scenarioName,
        numOfUsers: numOfUsers,
        numOfCalls: numOfCalls,
        iteration: iteration,
        firstNumber: firstNumber,
        lastExpectedCall: lastExpectedCall,
        callbacks: [],
        condition: condition,
    }
}

const initScenario = (request, response, scenarioBase) => {
    scenarioBase.callbacks.push(
        () => {
            response.end(JSON.stringify({result: `Load test iteration ${scenarioBase.iteration} is finished.`}));
            return;
        }
    );
    const iterationNum = getIteration(request);
    const isFirstIteration = (iterationNum == 0)
    if (isFirstIteration) {
        clearReport();
    }
    recordStartIteration(iterationNum);
    return scenarioBase;
}

/**
 * This function takes phone events from a puppeteer client and creates an
 * object that represents the event as a whole (eg. aggregates them).
 * The currentCall atribute expresses between which users is the ongoing call.
 * The callEvent contains the phoneEvent that has been recieved from puppeteer
 * clients.
 */
const agregateClientEvent = (userType, eventType, username, partyNumber, sipCallId = null) => {
    return {
        currentCall: (userType == 'caller') ?
            `call-${username}-${partyNumber}` :
            `call-${partyNumber}-${username}`,
        callEvent: (sipCallId) ?
            `${new Date().toISOString()}.${userType}.${eventType}.${sipCallId}` :
            `${new Date().toISOString()}.${userType}.${eventType}`,
    };
}

// const agregateClientEventOneSessionMode = (userType, eventType) => {
//     return {
//         callEvent: `${new Date().toISOString()}.${userType}.${eventType}`,
//     }
// }

const returnCallback = (scenario, iteration) => {
    console.log(`ClientProxy: It's the last call!`);
    recordStopIteration(iteration);
    if (scenario.callbacks.length == 1) {
        scenario.callbacks[0]();
    } else {
        console.error(`GOT EMPTY CALLBACK WHILE EXPECTING ONE! \n callback: ${callback}`);
    }
    scenario.callbacks = [];
}

const clearScenario = (ongoingScenarios, scenario) => {
    ongoingScenarios.splice(ongoingScenarios.indexOf(scenario), 1);
}

const notifyPendingRequests = (ongoingScenarios, callEvent, currentCall, iteration) => {
    ongoingScenarios.forEach(scenario => {
        const isLastExpectedCall = (currentCall == scenario.lastExpectedCall)
        const isLastExpectedPhoneEvent = callEvent.includes(scenario.condition)
        if (isLastExpectedCall && isLastExpectedPhoneEvent) {
            const bothSidesHangedUp = verifyPhoneEventExistsOnBothSides(iteration, currentCall, scenario.condition);
            if (bothSidesHangedUp) {
                returnCallback(scenario, iteration);
                clearScenario(ongoingScenarios, scenario);
            }
        }
    });
}

// const notifyPendingRequestsOneSessionMode = (ongoingScenarios, callEvent, currentCall = null) => {
//     Object.keys(ongoingScenarios).forEach(requestType => {
//         const callbackExists = ongoingScenarios[requestType]
//         const isLastExpectedCall = (currentCall)
//             ? (currentCall === LAST_EXPECTED_CALL) && (callEvent.includes(ongoingScenarios[requestType].condition))
//             : callEvent.includes(ongoingScenarios[requestType].condition)
//         if (callbackExists && isLastExpectedCall) {
//             setTimeout(() => {
//                 ongoingScenarios[requestType].callbacks[0]();
//                 ongoingScenarios[requestType].callbacks = [];
//             }, 5000);
//         }
//     });
// }

module.exports = {
    getUrlParams: getUrlParams,
    extractUrlParams: extractUrlParams,
    notifyPendingRequests: notifyPendingRequests,
    initScenario: initScenario,
    scenarioFactory: scenarioFactory,
    agregateClientEvent: agregateClientEvent,
    // agregateClientEventOneSessionMode: agregateClientEventOneSessionMode,
    // notifyPendingRequestsOneSessionMode: notifyPendingRequestsOneSessionMode,
}