let WEBRTC_LOADTEST_REPORT = {};

const sendReport = (res) => {
    console.log('ClientProxy: sending requested report');
    const report = JSON.stringify(WEBRTC_LOADTEST_REPORT);
    res.end(report);
    return;
}

const recordStartIteration = (iteration) => {
    const currentIteration = formatIteration(iteration);
    addPropertyToReportElement(WEBRTC_LOADTEST_REPORT, currentIteration, {});
    addPropertyToReportElement(WEBRTC_LOADTEST_REPORT[currentIteration], "start", new Date().toISOString());
}

const recordStopIteration = (iteration) => {
    const currentIteration = formatIteration(iteration);
    addPropertyToReportElement(WEBRTC_LOADTEST_REPORT[currentIteration], "stop", new Date().toISOString());
}

const recordCallEvent = (iteration, callEvent, currentCall = null) => {
    const currentIteration = formatIteration(iteration);
    addPropertyToReportElement(WEBRTC_LOADTEST_REPORT[currentIteration], currentCall, []);
    WEBRTC_LOADTEST_REPORT[currentIteration][currentCall].push(callEvent);
}

const addPropertyToReportElement = (reportElement, property, value) => {
    if (!reportElement.hasOwnProperty(property)) {
        console.log(`ClientProxy: Added property ${property} to loadtest report`);
        reportElement[property] = value;
    }
}

const formatIteration = (iterationNum) => {
    return `iteration.${iterationNum}`;
}

const verifyPhoneEventExistsOnBothSides = (iteration, call, callEvent) => {
    const formattedIteration = formatIteration(iteration);
    const callerEventExists = WEBRTC_LOADTEST_REPORT[formattedIteration][call].some(ce => ce.includes(`caller.${callEvent}`));
    const calleeEventExists = WEBRTC_LOADTEST_REPORT[formattedIteration][call].some(ce => ce.includes(`callee.${callEvent}`));
    const phoneEventAtBothSidesExists = callerEventExists && calleeEventExists;
    return phoneEventAtBothSidesExists;
}

const clearReport = () => {
    WEBRTC_LOADTEST_REPORT = {};
}

module.exports = {
    sendReport: sendReport,
    recordStartIteration: recordStartIteration,
    recordStopIteration: recordStopIteration,
    recordCallEvent: recordCallEvent,
    verifyPhoneEventExistsOnBothSides: verifyPhoneEventExistsOnBothSides,
    clearReport: clearReport,
}