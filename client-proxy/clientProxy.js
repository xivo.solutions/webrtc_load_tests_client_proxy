const http = require('http');
const { clientEvent, setupScenario } = require('./libs/callPairMode');
// const { simultaneousClientEvent, lastSimultaneousCall } = require('./libs/oneSessionMode');
const { sendReport } = require('./libs/reporter');

const CLIENT_PROXY_PORT = parseInt(process.env.CLIENT_PROXY_PORT);

const launchClientProxy = () => {
    const proxy = http.createServer(requestListener);
    proxy.listen(CLIENT_PROXY_PORT);
}

const requestListener = async (req, res) => {
    console.log(`ClientProxy: New request: ${req.url}`);
    const baseURL = `${req.protocol}://${req.headers.host}/`;
    const reqUrl = new URL(req.url, baseURL);
    const pathName = reqUrl.pathname

    if (pathName == "/report") clientEvent(req, res);
    if (pathName == "/setup") setupScenario(req, res);
    if (pathName == "/get") sendReport(res);

    // if (pathName == "/lastCallOneSession") lastSimultaneousCall(req, res);
    // if (pathName == "/reportOneSession") simultaneousClientEvent(req, res);
    // if (pathName == "/lastRegister") lastRegistration(req, res);
}

launchClientProxy();